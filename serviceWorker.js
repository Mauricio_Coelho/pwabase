const assets = [
    "index.html",
    "js/app.js"
]

const statDevPWA = "br.edu.cest.go-dev-1"
self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
        caches.open(statDevPWA) .then(
            cache => {
                cache.addAll(assets);
            }
        )
    )
}
)
self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWidth(
        caches.match(fetchEvent.request).then((resp) =>{
            return resp || fetch(fetchEvent.request)
            .then((response) => {
                return caches.open(statDevPWA)
                .then((cache) =>{
                    cache.put(fetchEvent.request, response.
clone());
                    return response;
                    }
                );
            });
        })
    );
    }
);

